/*
 * AutomaticTests.hpp
 *
 *  Created on: 15 kwi 2018
 *      Author: darek
 */

#ifndef AUTOMATICTESTS_HPP_
#define AUTOMATICTESTS_HPP_
#include <iostream>
#include <fstream>
#include <vector>
#include "Timer.hpp"
#include "Array.hpp"
#include "List.hpp"
#include "Heap.hpp"

class AutomaticTests {
	Array array;
	List list;
	Heap heap;

	//Tylko na potrzeby testowania
	std::vector<uint> rand_number = { 100, 200, 500, 1000, 2000, 5000, 10000,
			20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000 };
	Timer timer;
	std::string dataOut;
	std::string dataIn;
	std::ofstream outFile;
	std::ifstream inFile;
	uint test_it;
	int value;

public:
	AutomaticTests();
	virtual ~AutomaticTests();

	void startArrayTest();
	void startListTest();
	void startHeapTest();
	void startRbtTest();

private:
	void prepareFiles(std::string dataIn, std::string dataOut);

	void array_o1(std::string dataIn, std::string dataOut);
	void array_o2(std::string dataIn, std::string dataOut);
	void array_o3(std::string dataIn, std::string dataOut);
	void array_o4(std::string dataIn, std::string dataOut);
	void array_o5(std::string dataIn, std::string dataOut);
	void array_o6(std::string dataIn, std::string dataOut);
	void array_o7(std::string dataIn, std::string dataOut);
	void array_o8(std::string dataIn, std::string dataOut);

	void list_o1(std::string dataIn, std::string dataOut);
	void list_o2(std::string dataIn, std::string dataOut);
	void list_o3(std::string dataIn, std::string dataOut);
	void list_o4(std::string dataIn, std::string dataOut);
	void list_o5(std::string dataIn, std::string dataOut);
	void list_o6(std::string dataIn, std::string dataOut);
	void list_o7(std::string dataIn, std::string dataOut);
	void list_o8(std::string dataIn, std::string dataOut);
	void list_o9(std::string dataIn, std::string dataOut);

	void heap_o1(std::string dataIn, std::string dataOut);
	void heap_o2(std::string dataIn, std::string dataOut);
	void heap_o3(std::string dataIn, std::string dataOut);
	void heap_o4(std::string dataIn, std::string dataOut);
};

#endif /* AUTOMATICTESTS_HPP_ */
