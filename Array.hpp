/*
 * Array.hpp
 *
 *  Created on: 28 mar 2018
 *      Author: darek
 */

#ifndef ARRAY_HPP_
#define ARRAY_HPP_
#include <cstddef>
#include <ostream>
#include <string>
#include "Exceptions.hpp"

class Array {
	int* table;
	uint size;

public:
	Array();
	virtual ~Array();

	int& get(uint index);
	size_t getSize();

	void addFirstElem(int elem);
	void addLastElem(int elem);
	void addElem(uint index, int elem);

	void removeFirstElem();
	void removeLastElem();
	void removeElem(uint index);

	bool checkElem(int elem);

	void clear();

	int& operator [](uint index);
	//int* operator=(int* );
	friend std::ostream& operator <<(std::ostream& ostr, Array& obj);
};

#endif /* ARRAY_HPP_ */
