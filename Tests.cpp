/*
 * Tests.cpp
 *
 *  Created on: 7 kwi 2018
 *      Author: darek
 */

#include "Tests.hpp"
#include "RandGen.hpp"

Tests::Tests() {
}

Tests::~Tests() {
}

void Tests::table() {
	srand(time(NULL));
	Timer timer;
	Array array;
	int option = -1;
	int value;
	std::string dataOut;
	std::string dataIn;
	std::ofstream outFile;
	std::ifstream inFile;

	while (option != 0) {
		std::cout << "Wybierz funkcję tablicy:" << std::endl;
		std::cout << "    1. Dodaj na początek" << std::endl;
		std::cout << "    2. Dodaj na koniec" << std::endl;
		std::cout << "    3. Dodaj gdziekolwiek" << std::endl;
		std::cout << "    4. Usuń pierwszy" << std::endl;
		std::cout << "    5. Usuń ostatni" << std::endl;
		std::cout << "    6. Usuń którykolwiek" << std::endl;
		std::cout << "    7. Wyszukaj element" << std::endl;
		std::cout << "    8. Wydrukuj tablicę" << std::endl;
		std::cout << "    0. Powrót" << std::endl << std::endl;
		std::cout << "Wybór: ";
		std::cin >> option;

		if (option == 0)
			return;

		/*std::cout << "Plik zawierający dane wejściowe: ";
		 std::cin >> dataIn;
		 //std::cout << "Plik z wynikami testu: ";
		 //std::cin >> dataOut;
		 dataOut = "wyniki_tablica_o" + std::to_string(option) + "_" + dataIn;

		 inFile.open(dataIn, std::ios::in);
		 outFile.open(dataOut, std::ios::out | std::ios::trunc);

		 if (inFile.is_open()) {
		 std::cout << "Otwarto plik " << dataIn << std::endl;
		 } else {
		 std::cout << "Nie udało się otworzyć pliku wejściowego!"
		 << std::endl << std::endl;
		 continue;
		 }

		 if (outFile.is_open()) {
		 std::cout << "Otwarto plik " << dataOut << std::endl;
		 } else {
		 std::cout << "Nie udało się otworzyć pliku wyjściowego!"
		 << std::endl << std::endl;
		 inFile.close();
		 continue;
		 }*/
		int in;
		switch (option) {
		default:
			std::cout << "Błędny wybór!" << std::endl;
			/*inFile.close();
			 outFile.close();*/
			break;
		case 0:
			/*inFile.close();
			 outFile.close();*/
			return;
		case 1:
			std::cout << "Wprowadz: ";
			std::cin >> value;

			timer.startTimer();
			array.addFirstElem(value);
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 2:
			std::cout << "Wprowadz: ";
			std::cin >> value;

			timer.startTimer();
			array.addLastElem(value);
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 3:
			std::cout << "Wprowadz: ";
			std::cin >> value;

			std::cout << "indeks";
			std::cin >> in;

			timer.startTimer();
			array.addElem(in, value);

			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 4:
			timer.startTimer();
			array.removeFirstElem();
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;
			break;
		case 5:
			timer.startTimer();
			array.removeLastElem();
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;
			break;
		case 6:
			std::cout << "Wprowadz: ";
			std::cin >> value;

			timer.startTimer();
			array.removeElem(value);
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 7:
			std::cout << "Wprowadz: ";
			std::cin >> value;
			array.addLastElem(value);

			timer.startTimer();
			std::cout << array.checkElem(value) << std::endl;
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 8:
			timer.startTimer();
			std::cout << array << std::endl;
			timer.stopTimer();
			std::cout << "Czas wykonania: " << timer.getTimeUs() << " us"
					<< std::endl;
			outFile << timer.getTimeUs();
			break;
		}
		/*inFile.close();
		 outFile.close();*/
	}
}

void Tests::list() {
	srand(time(NULL));
	Timer timer;
	List list;
	int option = -1;
	int value;
	std::string dataIn;
	std::string dataOut;
	std::ofstream outFile;
	std::ifstream inFile;

	while (option != 0) {
		std::cout << "Wybierz funkcję listy:" << std::endl;
		std::cout << "    1. Dodaj na początek" << std::endl;
		std::cout << "    2. Dodaj na koniec" << std::endl;
		std::cout << "    3. Dodaj gdziekolwiek" << std::endl;
		std::cout << "    4. Usuń pierwszy" << std::endl;
		std::cout << "    5. Usuń ostatni" << std::endl;
		std::cout << "    6. Usuń którykolwiek" << std::endl;
		std::cout << "    7. Wyszukaj element" << std::endl;
		std::cout << "    8. Wydrukuj listę" << std::endl;
		std::cout << "    9. Usuń po wartości" << std::endl;
		std::cout << "    0. Powrót" << std::endl << std::endl;
		std::cout << "Wybór: ";
		std::cin >> option;

		if (option == 0)
			return;

		/*std::cout << "Plik zawierający dane wejściowe: ";
		 std::cin >> dataIn;*/
		//std::cout << "Plik z wynikami testu: ";
		//std::cin >> dataOut;
		/*dataOut = "wyniki_lista_o" + std::to_string(option) + "_" + dataIn;

		 inFile.open(dataIn, std::ios::in);
		 outFile.open(dataOut, std::ios::out | std::ios::trunc);

		 if (inFile.is_open()) {
		 std::cout << "Otwarto plik " << dataIn << std::endl;
		 } else {
		 std::cout << "Nie udało się otworzyć pliku wejściowego!"
		 << std::endl << std::endl;
		 continue;
		 }

		 if (outFile.is_open()) {
		 std::cout << "Otwarto plik " << dataOut << std::endl;
		 } else {
		 std::cout << "Nie udało się otworzyć pliku wyjściowego!"
		 << std::endl << std::endl;
		 inFile.close();
		 continue;
		 }*/
		int in;
		switch (option) {
		default:
			std::cout << "Błędny wybór!" << std::endl;
			inFile.close();
			outFile.close();
			break;
		case 0:
			inFile.close();
			outFile.close();
			return;
		case 1:
			std::cout << "Wprowadz: ";
			std::cin >> value;

			timer.startTimer();
			list.addFirstElem(value);
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 2:
			std::cout << "Wprowadz: ";
			std::cin >> value;

			timer.startTimer();
			list.addLastElem(value);
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 3:
			std::cout << "Wprowadz: ";
			std::cin >> value;
			std::cout << "indeks";
			std::cin >> in;

			timer.startTimer();
			list.addElem(in, value);
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 4:
			timer.startTimer();
			list.removeFirstElem();
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 5:
			timer.startTimer();
			list.removeLastElem();
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 6:
			std::cout << "Wprowadz: ";
			std::cin >> value;
			list.addLastElem(value);

			timer.startTimer();
			list.removeElemByIndex(value);
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 7:
			std::cout << "Wprowadz: ";
			std::cin >> value;

			timer.startTimer();
			std::cout << list.checkElem(value) << std::endl;
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 8:

			timer.startTimer();
			std::cout << list << std::endl;
			timer.stopTimer();
			std::cout << "Czas wykonania: " << timer.getTimeUs() << " us"
					<< std::endl;
			outFile << timer.getTimeUs();
			break;
		case 9:

			std::cout << "Wprowadz: ";
			std::cin >> value;

			timer.startTimer();
			list.removeElemByValue(value);
			timer.stopTimer();
			std::cout << "Czas wykonania: " << timer.getTimeUs() << " us"
					<< std::endl;
			outFile << timer.getTimeUs();
			break;
		}
		/*inFile.close();
		 outFile.close();*/
	}

}

void Tests::heap() {
	srand(time(NULL));
	Timer timer;
	Heap heap;
	int option = -1;
	int value;
	std::string dataIn;
	std::string dataOut;
	std::ofstream outFile;
	std::ifstream inFile;

	while (option != 0) {
		std::cout << "Wybierz funkcję Kopca:" << std::endl;
		std::cout << "    1. Dodaj" << std::endl;
		std::cout << "    2. Usuń wybrany element po wartości" << std::endl;
		std::cout << "    3. Wyszukaj" << std::endl;
		std::cout << "    4. Wydrukuj kopiec" << std::endl;
		std::cout << "    5. Usuń wybrany element po indeksie" << std::endl;
		std::cout << "    0. Powrót" << std::endl << std::endl;
		std::cout << "Wybór: ";
		std::cin >> option;

		if (option == 0)
			return;

		/*std::cout << "Plik zawierający dane wejściowe: ";
		 std::cin >> dataIn;
		 //std::cout << "Plik z wynikami testu: ";
		 //std::cin >> dataOut;
		 dataOut = "wyniki_kopiec_o" + std::to_string(option) + "_" + dataIn;

		 inFile.open(dataIn, std::ios::in);
		 outFile.open(dataOut, std::ios::out | std::ios::trunc);

		 if (inFile.is_open()) {
		 std::cout << "Otwarto plik " << dataIn << std::endl;
		 } else {
		 std::cout << "Nie udało się otworzyć pliku wejściowego!"
		 << std::endl << std::endl;
		 continue;
		 }

		 if (outFile.is_open()) {
		 std::cout << "Otwarto plik " << dataOut << std::endl;
		 } else {
		 std::cout << "Nie udało się otworzyć pliku wyjściowego!"
		 << std::endl << std::endl;
		 inFile.close();
		 continue;
		 }
		 */
		switch (option) {
		default:
			std::cout << "Błędny wybór!" << std::endl;
			//inFile.close();
			//outFile.close();
			break;
		case 0:
			//inFile.close();
			//outFile.close();
			return;
		case 1:
			std::cout << "Wprowadz: ";
			std::cin >> value;

			timer.startTimer();
			heap.addElem(value);
			timer.stopTimer();

			outFile << timer.getTimeNs() << std::endl;

			break;
		case 2:
			std::cout << "Wprowadz: ";
			std::cin >> value;
			timer.startTimer();
			heap.removeElem(value);
			timer.stopTimer();
			/*while (heap.getSize() != 0) {
			 timer.startTimer();
			 while (!heap.removeElem(rand() % 2000000 - 1000000))
			 ;
			 timer.stopTimer();
			 }*/
			outFile << timer.getTimeNs() << std::endl;
			break;
		case 3:
			std::cout << "Wprowadz: ";
			std::cin >> value;
			timer.startTimer();
			std::cout << heap.checkElem(value) << std::endl;
			timer.stopTimer();
			/*for (uint i = 0; i < 100000; i++) {
			 timer.startTimer();
			 while (!heap.checkElem(rand() % 2000000 - 1000000))
			 ;
			 timer.stopTimer();
			 }*/
			outFile << timer.getTimeNs() << std::endl;
			break;
		case 4:

			timer.startTimer();
			heap.print();
			//std::cout << heap << std::endl;
			timer.stopTimer();
			std::cout << "Czas wykonania: " << timer.getTimeUs() << " us"
					<< std::endl;
			outFile << timer.getTimeUs();
			break;
		case 5:
			std::cout << "Wprowadz: ";
			std::cin >> value;
			timer.startTimer();
			heap.removeIndex(value);
			timer.stopTimer();
			outFile << timer.getTimeNs() << std::endl;
			break;
		}

		/*while (heap.getSize() != 0) {
		 timer.startTimer();
		 while (!heap.removeElem(rand() % 2000000 - 1000000))
		 ;
		 timer.stopTimer();
		 }*/

		//inFile.close();
		//outFile.close();
	}
}
