/*
 * Array.cpp
 *
 *  Created on: 28 mar 2018
 *      Author: darek
 */

#include "Array.hpp"

Array::Array() :
		table(nullptr), size(0) {
}

Array::~Array() {
	if (table == nullptr || size != 0)
		delete[] table;
}

int& Array::get(uint index) {
	return table[index];
}

size_t Array::getSize() {
	return size;
}

void Array::addFirstElem(int elem) {
	addElem(0, elem);
}

void Array::addLastElem(int elem) {
	addElem(size, elem);
}

void Array::addElem(uint index, int elem) {
	if (index > size) {
		throw RangeExceeded("RangeExceeded");
	}

	if (size == 0) {
		table = new int[size + 1];
		table[index] = elem;
		size++;
		return;
	}

	int* new_table = new int[size + 1];
	int* tmp_new = new_table;
	int* tmp_old = table;
	for (uint i = 0; i < size; i++) {
		if (i == index)
			tmp_new++;
		*tmp_new = *tmp_old;
		tmp_new++;
		tmp_old++;
	}
	new_table[index] = elem;
	delete[] table;
	table = new_table;
	size++;
}

void Array::removeFirstElem() {
	removeElem(0);
}

void Array::removeLastElem() {
	removeElem(size - 1);
}

void Array::removeElem(uint index) {
	if (index >= size)
		throw RangeExceeded("RangeExceeded");

	int* new_table = new int[size - 1];
	int* tmp_new = new_table;
	int* tmp_old = table;
	for (uint i = 0; i < size - 1; i++) {
		if (i == index)
			tmp_old++;
		*tmp_new = *tmp_old;
		tmp_new++;
		tmp_old++;
	}
	delete[] table;
	table = new_table;
	size--;
}

bool Array::checkElem(int elem) {
	for (size_t i = 0; i < size; i++) {
		if (get(i) == elem) {
			return true;
		}
	}
	return false;
}

void Array::clear() {
	if (table == nullptr || size != 0)
		delete[] table;
	table = nullptr;
	size = 0;
}

int& Array::operator [](uint index) {
	return table[index];
}

std::ostream & operator <<(std::ostream & ostr, Array & obj) {
	ostr << "[";
	for (uint i = 0; i < obj.size; i++) {
		ostr << obj[i];
		if (i < obj.size - 1)
			ostr << ", ";
	}
	ostr << "]" << std::endl;
	return ostr;
}
