/*
 * Heap.cpp
 *
 *  Created on: 28 mar 2018
 *      Author: darek
 */

#include "Heap.hpp"
#include <math.h>

Heap::Heap() :
		table(nullptr), size(0) {
}

Heap::~Heap() {
	clear();
}

size_t Heap::getSize() {
	return this->size;
}

void Heap::addElem(int elem) {
	if (!checkElem(elem)) {
		int* new_table = new int[size + 1];
		if (table == nullptr) {
			table = new_table;
			table[0] = elem;
		} else {
			for (size_t i = 0; i < size; i++) {
				new_table[i] = table[i];
			}
			delete[] table;
			new_table[size] = elem;
			table = new_table;
		}
		construct();
		size++;
	}
}

void Heap::removeIndex(int index) {
	if (index >= size)
		throw RangeExceeded("RangeExceeded");

	int* new_table = new int[size - 1];
	int* tmp_new = new_table;
	int* tmp_old = table;
	for (uint i = 0; i < size - 1; i++) {
		if (i == index)
			tmp_old++;
		*tmp_new = *tmp_old;
		tmp_new++;
		tmp_old++;
	}
	delete[] table;
	table = new_table;
	size--;
}

bool Heap::removeElem(int elem) {
	for (uint i = 0; i < size; i++) {
		if (table[i] == elem) {
			int *newTable = new int[size - 1];
			for (uint k = 0; k < i; k++) {
				newTable[k] = table[k];
			}

			for (uint k = i + 1; k < size; k++) {
				newTable[k - 1] = table[k];
			}

			delete[] table;

			table = newTable;

			size--;

			construct();
			return true;
		}
	}

	return false;
}

bool Heap::checkElem(int elem) {
	for (size_t i = 0; i < size; i++) {
		if (table[i] == elem) {
			return true;
		}
	}
	return false;
}

void Heap::construct() {
	int tmp = 0;
	for (size_t i = size; 0 < i; i--) {
		if (table[i - 1] < table[i]) {
			tmp = table[i - 1];
			table[i - 1] = table[i];
			table[i] = tmp;
		}
	}
}

void Heap::clear() {
	if (size > 0) {
		if (table != nullptr)
			delete[] table;
		table = nullptr;
		size = 0;
	}
}

void Heap::print(std::string sp, std::string sn, uint v) {
	std::string cr, cl, cp, s;
	cr = cl = cp = "    ";
	/*cr[0] = 218;
	 cr[1] = 196;
	 cl[0] = 192;
	 cl[1] = 196;
	 cp[0] = 179;*/

	if (v < size) {
		s = sp;
		if (sn == cr)
			s[s.length() - 2] = ' ';
		print(s + cp, cr, 2 * v + 2);

		s = s.substr(0, sp.length() - 2);

		std::cout << s << sn << table[v] << std::endl;

		s = sp;
		if (sn == cl)
			s[s.length() - 2] = ' ';
		print(s + cp, cl, 2 * v + 1);
	}

}
