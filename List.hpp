/*
 * List.hpp
 *
 *  Created on: 28 mar 2018
 *      Author: darek
 */

#ifndef LIST_HPP_
#define LIST_HPP_
#include <cstddef>
#include <ostream>
#include <string>
#include "Exceptions.hpp"

class List {
public:
	struct Element {
		int element = 0;
		Element* next = nullptr;
		Element* prev = nullptr;
	};

private:
	size_t size;
	Element* first_elem;
	Element* last_elem;

public:
	List();
	virtual ~List();

	size_t getSize();

	int& get(uint index);

	void addFirstElem(int elem);
	void addLastElem(int elem);
	void addElem(uint index, int elem);

	void removeFirstElem();
	void removeLastElem();
	void removeElemByIndex(uint index);
	void removeElemByValue(int value);

	bool checkElem(int elem);

	void clear();

	int& operator [](uint index);
	//int* operator=(int* );
	friend std::ostream& operator <<(std::ostream& ostr, List& obj);
};

#endif /* LIST_HPP_ */
