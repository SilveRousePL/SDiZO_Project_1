/*
 * AutomaticTests.cpp
 *
 *  Created on: 15 kwi 2018
 *      Author: darek
 */

#include "AutomaticTests.hpp"
#include <unistd.h>

AutomaticTests::AutomaticTests() :
		test_it(0), value(0) {

}

AutomaticTests::~AutomaticTests() {
	if (inFile.is_open())
		inFile.close();
	if (outFile.is_open())
		outFile.close();
}

void AutomaticTests::startArrayTest() {
	std::cout << "Rozpoczynam test dla tablicy" << std::endl;
	for (test_it = 0; test_it < rand_number.size(); test_it++) {
		std::cout << "Rozpoczynam test tablicy dla L" << std::to_string(rand_number[test_it]) << "nr 1..." << std::endl;
		array_o1(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_tablica_o1_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test tablicy dla L" << std::to_string(rand_number[test_it]) << "nr 2..." << std::endl;
		array_o2(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_tablica_o2_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test tablicy dla L" << std::to_string(rand_number[test_it]) << "nr 3..." << std::endl;
		array_o3(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_tablica_o3_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test tablicy dla L" << std::to_string(rand_number[test_it]) << "nr 4..." << std::endl;
		array_o4(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_tablica_o4_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test tablicy dla L" << std::to_string(rand_number[test_it]) << "nr 5..." << std::endl;
		array_o5(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_tablica_o5_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test tablicy dla L" << std::to_string(rand_number[test_it]) << "nr 6..." << std::endl;
		array_o6(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_tablica_o6_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test tablicy dla L" << std::to_string(rand_number[test_it]) << "nr 7..." << std::endl;
		array_o7(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_tablica_o7_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test tablicy dla L" << std::to_string(rand_number[test_it]) << "nr 8..." << std::endl;
		array_o8(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_tablica_o8_L")
						+ std::to_string(rand_number[test_it]));
	}
}

void AutomaticTests::startListTest() {
	std::cout << "Rozpoczynam test dla listy" << std::endl;
	for (test_it = 0; test_it < rand_number.size(); test_it++) {
		std::cout << "Rozpoczynam test listy dla L" << std::to_string(rand_number[test_it]) << "nr 1..." << std::endl;
		list_o1(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_lista_o1_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test listy dla L" << std::to_string(rand_number[test_it]) << "nr 2..." << std::endl;
		list_o2(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_lista_o2_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test listy dla L" << std::to_string(rand_number[test_it]) << "nr 3..." << std::endl;
		list_o3(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_lista_o3_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test listy dla L" << std::to_string(rand_number[test_it]) << "nr 4..." << std::endl;
		list_o4(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_lista_o4_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test listy dla L" << std::to_string(rand_number[test_it]) << "nr 5..." << std::endl;
		list_o5(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_lista_o5_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test listy dla L" << std::to_string(rand_number[test_it]) << "nr 6..." << std::endl;
		list_o6(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_lista_o6_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test listy dla L" << std::to_string(rand_number[test_it]) << "nr 7..." << std::endl;
		list_o7(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_lista_o7_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test listy dla L" << std::to_string(rand_number[test_it]) << "nr 8..." << std::endl;
		list_o8(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_lista_o8_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test listy dla L" << std::to_string(rand_number[test_it]) << "nr 9..." << std::endl;
		list_o9(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_lista_o9_L")
						+ std::to_string(rand_number[test_it]));
	}
}

void AutomaticTests::startHeapTest() {
	std::cout << "Rozpoczynam test dla kopca" << std::endl;
	for (test_it = 0; test_it < rand_number.size(); test_it++) {
		std::cout << "Rozpoczynam test kopca dla L" << std::to_string(rand_number[test_it]) << "nr 1..." << std::endl;
		heap_o1(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_kopiec_o1_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test kopca dla L" << std::to_string(rand_number[test_it]) << "nr 2..." << std::endl;
		heap_o2(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_kopiec_o2_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test kopca dla L" << std::to_string(rand_number[test_it]) << "nr 3..." << std::endl;
		heap_o3(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_kopiec_o3_L")
						+ std::to_string(rand_number[test_it]));
		std::cout << "Rozpoczynam test kopca dla L" << std::to_string(rand_number[test_it]) << "nr 4..." << std::endl;
		heap_o4(std::string("L") + std::to_string(rand_number[test_it]),
				std::string("wyniki_kopiec_o4_L")
						+ std::to_string(rand_number[test_it]));
	}
}

void AutomaticTests::startRbtTest() {
}

void AutomaticTests::prepareFiles(std::string dataIn, std::string dataOut) {
	if (inFile.is_open())
		inFile.close();
	if (outFile.is_open())
		outFile.close();
	inFile.open(dataIn, std::ios::in);
	outFile.open(dataOut, std::ios::out | std::ios::trunc);
	if (!inFile.is_open()) {
		std::cout << "Nie udało się otworzyć pliku wejściowego!" << std::endl
				<< std::endl;
		return;
	}
	if (!outFile.is_open()) {
		std::cout << "Nie udało się otworzyć pliku wyjściowego!" << std::endl
				<< std::endl;
		inFile.close();
		return;
	}
	this->dataIn = dataIn;
	this->dataOut = dataOut;
}

void AutomaticTests::array_o1(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		timer.startTimer();
		array.addFirstElem(value);
		timer.stopTimer();
		outFile << timer.getTimeNs() << std::endl;
	}
	array.clear();
}

void AutomaticTests::array_o2(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		timer.startTimer();
		array.addLastElem(value);
		timer.stopTimer();
		outFile << timer.getTimeNs() << std::endl;
	}
	array.clear();
}

void AutomaticTests::array_o3(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		timer.startTimer();
		array.addElem(rand() % (array.getSize() + 1), value);
		timer.stopTimer();
		outFile << timer.getTimeNs() << std::endl;
	}
	array.clear();
}

void AutomaticTests::array_o4(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		array.addLastElem(value);
	}
	while (array.getSize() != 0) {
		timer.startTimer();
		array.removeFirstElem();
		timer.stopTimer();
		outFile << timer.getTimeNs() << std::endl;
	}
	array.clear();
}

void AutomaticTests::array_o5(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		array.addLastElem(value);
	}
	while (array.getSize() != 0) {
		timer.startTimer();
		array.removeLastElem();
		timer.stopTimer();
		outFile << timer.getTimeNs() << std::endl;
	}
	array.clear();
}

void AutomaticTests::array_o6(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		array.addLastElem(value);
	}
	while (array.getSize() != 0) {
		timer.startTimer();
		array.removeElem(rand() % array.getSize());
		timer.stopTimer();
		outFile << timer.getTimeNs() << std::endl;
	}
	array.clear();
}

void AutomaticTests::array_o7(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		array.addLastElem(value);
	}
	for (uint i = 0; i < array.getSize(); i++) {
		timer.startTimer();
		array.checkElem(value);
		timer.stopTimer();

		outFile << timer.getTimeNs() << std::endl;
	}
	array.clear();
}

void AutomaticTests::array_o8(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		array.addLastElem(value);
	}
	timer.startTimer();
	std::cout << array << std::endl;
	timer.stopTimer();
	outFile << timer.getTimeUs();
	array.clear();
}

void AutomaticTests::list_o1(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		timer.startTimer();
		list.addFirstElem(value);
		timer.stopTimer();
		outFile << timer.getTimeNs() << std::endl;
	}
	list.clear();
}

void AutomaticTests::list_o2(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		timer.startTimer();
		list.addLastElem(value);
		timer.stopTimer();
		outFile << timer.getTimeNs() << std::endl;
	}
	list.clear();
}

void AutomaticTests::list_o3(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		timer.startTimer();
		list.addElem(rand() % (list.getSize() + 1), value);
		timer.stopTimer();
		outFile << timer.getTimeNs() << std::endl;
	}
	list.clear();
}

void AutomaticTests::list_o4(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		list.addLastElem(value);
	}
	while (list.getSize() != 0) {
		timer.startTimer();
		list.removeFirstElem();
		timer.stopTimer();
		outFile << timer.getTimeNs() << std::endl;
	}
	list.clear();
}

void AutomaticTests::list_o5(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		list.addLastElem(value);
	}
	while (list.getSize() != 0) {
		timer.startTimer();
		list.removeLastElem();
		timer.stopTimer();

		outFile << timer.getTimeNs() << std::endl;
	}
	list.clear();
}

void AutomaticTests::list_o6(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		list.addLastElem(value);
	}
	while (list.getSize() != 0) {
		timer.startTimer();
		list.removeElemByIndex(rand() % list.getSize());
		timer.stopTimer();

		outFile << timer.getTimeNs() << std::endl;
	}
	list.clear();
}

void AutomaticTests::list_o7(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		list.addLastElem(value);
	}
	for (uint i = 0; i < list.getSize(); i++) {
		timer.startTimer();
		list.checkElem(value);
		timer.stopTimer();

		outFile << timer.getTimeNs() << std::endl;
	}
	list.clear();
}

void AutomaticTests::list_o8(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		list.addLastElem(value);
	}
	timer.startTimer();
	std::cout << list << std::endl;
	timer.stopTimer();
	outFile << timer.getTimeUs();
	list.clear();
}

void AutomaticTests::list_o9(std::string dataIn, std::string dataOut) {
	while (!inFile.eof()) {
		inFile >> value;
		list.addLastElem(value);
	}
	timer.startTimer();
	list.removeElemByValue(value);
	timer.stopTimer();
	outFile << timer.getTimeUs();
}

void AutomaticTests::heap_o1(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		timer.startTimer();
		heap.addElem(value);
		timer.stopTimer();
		outFile << timer.getTimeNs() << std::endl;
	}
	heap.clear();
}

void AutomaticTests::heap_o2(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		heap.addElem(value);
	}
	timer.startTimer();
	heap.removeElem(value);
	timer.stopTimer();
	outFile << timer.getTimeNs() << std::endl;
	heap.clear();
}

void AutomaticTests::heap_o3(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		heap.addElem(value);
	}
	timer.startTimer();
	heap.checkElem(value);
	timer.stopTimer();
	outFile << timer.getTimeNs() << std::endl;
	heap.clear();
}

void AutomaticTests::heap_o4(std::string dataIn, std::string dataOut) {
	prepareFiles(dataIn, dataOut);
	while (!inFile.eof()) {
		inFile >> value;
		heap.addElem(value);
	}
	timer.startTimer();
	heap.print();
	timer.stopTimer();
	outFile << timer.getTimeUs();
	heap.clear();
}
