################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Array.cpp \
../AutomaticTests.cpp \
../Heap.cpp \
../List.cpp \
../RBTree.cpp \
../Tests.cpp \
../main.cpp 

OBJS += \
./Array.o \
./AutomaticTests.o \
./Heap.o \
./List.o \
./RBTree.o \
./Tests.o \
./main.o 

CPP_DEPS += \
./Array.d \
./AutomaticTests.d \
./Heap.d \
./List.d \
./RBTree.d \
./Tests.d \
./main.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


