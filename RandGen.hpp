/*
 * RandGen.hpp
 *
 *  Created on: 7 kwi 2018
 *      Author: darek
 */

#ifndef RANDGEN_HPP_
#define RANDGEN_HPP_
#include <fstream>

class RandGen {
public:
	RandGen() {
		srand(time(NULL));
	}
	virtual ~RandGen() {

	}

	bool generate(std::string filename = "Lxxx", uint number = 1000,
			int32_t range_from = -1000000, int32_t range_to = 1000000) {
		if (filename.length() == 0)
			return false;

		size_t xxx_pos = filename.find("xxx");
		if (xxx_pos != std::string::npos) {
			filename.erase(xxx_pos, 3);
			filename.insert(xxx_pos, std::to_string(number));
		}

		std::fstream handle;
		handle.open(filename, std::ios::out | std::ios::trunc);
		if (!handle.is_open())
			return false;
		for (uint i = 0; i < number; i++) {
			handle << (rand() % (range_to - range_from)) + range_from << std::endl;
		}
		handle.close();
		return true;
	}

};

#endif /* RANDGEN_HPP_ */
