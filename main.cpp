/*
 * main.cpp
 *
 *  Created on: 28 mar 2018
 *      Author: darek
 */
#include <iostream>
#include "Tests.hpp"
#include "AutomaticTests.hpp"
#include "RandGen.hpp"

int main(int argc, char **argv) {
	RandGen rgen;
	Tests tests;
	AutomaticTests autotests;
	int option = -1;

	/*List list;
	while (true) {
		for (int i = 1; i <= 10; i++)
			list.addLastElem(i);
		for (int i = 1; i <= 10; i++) {
			int j = rand() % list.getSize();
			std::cout << "list[" << j << "]: " << std::endl;
			std::cout << list[j] << std::endl;
			list.removeElemByIndex(j);
		}
		list.clear();
	}*/

	std::cout << "Struktury Danych i Złożoność Obliczeniowa - Projekt 1"
			<< std::endl << "Dariusz Tomaszewski, 235565" << std::endl
			<< "Data i czas kompilacji: " << __DATE__ << " " << __TIME__ << " "
			<< std::endl << std::endl;

	while (option != 0) {
		std::cout << "Wybierz test:" << std::endl;
		std::cout << "    1. Tablica" << std::endl;
		std::cout << "    2. Lista" << std::endl;
		std::cout << "    3. Kopiec Binarny" << std::endl;
		std::cout << "    4. Drzewo Czerowno-Czarne" << std::endl << std::endl;
		std::cout << "    8. Automatyczne testy" << std::endl;
		std::cout << "    9. Generuj plik z losowymi wartościami" << std::endl;
		std::cout << "    0. Wyjście" << std::endl << std::endl;
		std::cout << "Wybór: ";
		std::cin >> option;

		if (option == 0)
			return 0;

		switch (option) {
		default:
			std::cout << "Błędny wybór!" << std::endl;
			break;
		case 0:
			break;
		case 1:
			tests.table();
			break;
		case 2:
			tests.list();
			break;
		case 3:
			tests.heap();
			break;
		case 4:
			//tests.RBT();
			break;
		case 8:
			autotests.startArrayTest();
			autotests.startListTest();
			autotests.startHeapTest();
			break;
		case 9:

			uint n;
			std::cout << "Ilość: ";
			std::cin >> n;
			/* int32_t from;
			 int32_t to;
			 std::cout << "Wprowadź kolejno: ilość   od   do" << std::endl
			 << "                  ";
			 std::cin >> n >> from >> to;*/
			rgen.generate("Lxxx", n, -1000000, 1000000);
			break;
		}
	}
	return 0;
}
