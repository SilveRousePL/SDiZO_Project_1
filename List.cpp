/*
 * List.cpp
 *
 *  Created on: 28 mar 2018
 *      Author: darek
 */

#include "List.hpp"

List::List() :
		size(0), first_elem(nullptr), last_elem(nullptr) {
	// TODO Auto-generated constructor stub
}

List::~List() {
	this->clear();
}

size_t List::getSize() {
	return this->size;
}

int& List::get(uint index) {
	Element* current_elem = nullptr;
	if (index < size / 2) {
		current_elem = first_elem;
		for (uint i = 0; i < index; i++) {
			current_elem = current_elem->next;
		}
	} else {
		current_elem = last_elem;
		for (uint i = 0; i < size - index - 1; i++) {
			current_elem = current_elem->prev;
		}
	}
	return current_elem->element;
}

void List::addFirstElem(int elem) {
	if (first_elem == nullptr) {
		first_elem = new Element;
		last_elem = first_elem;
	} else {
		Element* current_elem = new Element;
		current_elem->next = first_elem;
		first_elem->prev = current_elem;
		first_elem = current_elem;
	}
	first_elem->element = elem;
	size++;
}

void List::addLastElem(int elem) {
	if (last_elem == nullptr) {
		last_elem = new Element;
		first_elem = last_elem;
	} else {
		Element* current_elem = new Element;
		current_elem->prev = last_elem;
		last_elem->next = current_elem;
		last_elem = current_elem;
	}
	last_elem->element = elem;
	size++;
}

void List::addElem(uint index, int elem) {
	if (index < 0 || index > size)
		throw RangeExceeded("RangeExceeded");
	if (index == 0) {
		addFirstElem(elem);
		return;
	}
	if (index == size) {
		addLastElem(elem);
		return;
	}

	Element* current_elem = nullptr;
	if (index < size / 2) {
		current_elem = first_elem;
		for (uint i = 0; i < index; i++) {
			current_elem = current_elem->next;
		}
	} else {
		current_elem = last_elem;
		for (uint i = 0; i < size - index - 1; i++) {
			current_elem = current_elem->prev;
		}
	}

	Element* new_element = new Element;
	new_element->element = elem;
	new_element->next = current_elem;
	new_element->prev = current_elem->prev;

	current_elem->prev->next = new_element;
	current_elem->prev = new_element;

	size++;
}

void List::removeFirstElem() {
	if (size == 1) {
		delete first_elem;
		first_elem = nullptr;
		last_elem = nullptr;
		size--;
		return;
	}
	Element* current_elem = first_elem;
	current_elem = current_elem->next;
	current_elem->prev = nullptr;
	delete first_elem;
	first_elem = current_elem;
	size--;

	/*
	 if (first_elem == nullptr)
	 return;
	 Element* tmp_pointer_next = first_elem->next;
	 delete first_elem;
	 first_elem = tmp_pointer_next;
	 if (first_elem == nullptr)
	 last_elem = nullptr;
	 size--;
	 */
}

void List::removeLastElem() {
	if (size == 1) {
		delete first_elem;
		first_elem = nullptr;
		last_elem = nullptr;
		size--;
		return;
	}
	Element* current_elem = last_elem;
	current_elem = current_elem->prev;
	current_elem->next = nullptr;
	delete last_elem;
	last_elem = current_elem;
	size--;

	/*if (last_elem == nullptr)
	 return;
	 Element* tmp_pointer_prev = last_elem->prev;
	 delete last_elem;
	 last_elem = tmp_pointer_prev;
	 if (last_elem == nullptr)
	 first_elem = nullptr;
	 size--;*/
}

void List::removeElemByIndex(uint index) {
	if (index < 0 || index > size)
		throw RangeExceeded("RangeExceeded");
	if (index == 0) {
		removeFirstElem();
		return;
	}
	if (index == size - 1) {
		removeLastElem();
		return;
	}

	Element* current_elem = nullptr;

	if (index < size / 2) {
		current_elem = first_elem;
		for (uint i = 0; i < index; i++) {
			current_elem = current_elem->next;
		}
	} else {
		current_elem = last_elem;
		for (uint i = 0; i < size - index - 1; i++) {
			current_elem = current_elem->prev;
		}
	}

	current_elem->next->prev = current_elem->prev;
	current_elem->prev->next = current_elem->next;

	delete current_elem;
	current_elem = first_elem;
	size--;
}

void List::removeElemByValue(int value) {
	if (size == 0)
		return;
	Element* current_elem = first_elem;
	for (uint i = 0; i < size; i++) {
		if (current_elem->element == value) {
			removeElemByIndex(i);
			return;
		}
		current_elem = current_elem->next;
	}
}

bool List::checkElem(int elem) {
	if (size == 0)
		return false;
	Element* current_elem = first_elem;
	for (uint i = 0; i < size; i++) {
		if (current_elem->element == elem)
			return true;

		current_elem = current_elem->next;
	}
	return false;
}

void List::clear() {
	Element* tmp_pointer = first_elem;
	while (tmp_pointer != nullptr) {
		Element* to_remove = tmp_pointer;
		tmp_pointer = tmp_pointer->next;
		delete to_remove;
	}
	first_elem = nullptr;
	last_elem = nullptr;
	size = 0;
}

int& List::operator [](uint index) {
	return get(index);
}

std::ostream & operator <<(std::ostream & ostr, List & obj) {
	ostr << "[";
	for (uint i = 0; i < obj.size; i++) {
		ostr << obj[i];
		if (i < obj.size - 1)
			ostr << ", ";
	}
	ostr << "]" << std::endl;
	return ostr;
}
