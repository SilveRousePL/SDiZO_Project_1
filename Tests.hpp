/*
 * Tests.hpp
 *
 *  Created on: 7 kwi 2018
 *      Author: darek
 */

#ifndef TESTS_HPP_
#define TESTS_HPP_
#include <iostream>
#include <fstream>
#include "Timer.hpp"
#include "Array.hpp"
#include "List.hpp"
#include "Heap.hpp"

class Tests {
public:
	Tests();
	virtual ~Tests();
    void table();
    void list();
    void heap();
};

#endif /* TESTS_HPP_ */
