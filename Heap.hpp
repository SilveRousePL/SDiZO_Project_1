/*
 * Heap.hpp
 *
 *  Created on: 28 mar 2018
 *      Author: darek
 */

#ifndef HEAP_HPP_
#define HEAP_HPP_
#include <cstddef>
#include <ostream>
#include <string>
#include "Exceptions.hpp"

class Heap {
	int* table;
	size_t size;

public:
	Heap();
	virtual ~Heap();
	size_t getSize();
	void addElem(int elem);
	void removeIndex(int index);
	bool removeElem(int elem);
	bool checkElem(int elem);
	void construct();
	void clear();
	void print(std::string sp = "", std::string sn = "", uint v = 0);
};

#endif /* HEAP_HPP_ */
